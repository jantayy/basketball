﻿using BasketballProject.Enums;
using BasketballProject.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BasketballProject.Models
{
    public class Player : Person
    {
        public int Height { get; set; } // я в сантиметрах рост указываю
        public decimal Weight { get; set; }
        [Required]
        public int JerseyNumber { get; set; }
        [Required]
        public Position Position { get; set; }
    }
}
