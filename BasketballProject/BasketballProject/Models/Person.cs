﻿using BasketballProject.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BasketballProject.Models
{
    public abstract class Person : IEntity<int> 
    {
        public int Id { get; set; }
        [ForeignKey("Team")]
        public int TeamId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Surname { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
        public virtual Team Team { get; set; }
    }
}
