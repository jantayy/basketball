﻿using BasketballProject.Enums;
using BasketballProject.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BasketballProject.Models
{
    public class Team : IEntity<int>
    {
        public int Id { get; set; }

        [ForeignKey("Player")]
        public int PlayerId { get; set; }

        [ForeignKey("Coach")]
        public int CoachId { get; set; }

        [Required]
        public string Name { get; set; }
        public string City { get; set; }

        [Required]
        public Conference Conference { get; set; }

        [Required]
        public int YearOfCreation { get; set; }
        public int TotalWins { get; set; }
        public int TotalLoses { get; set; }
        public virtual Coach Coach { get; set; }
        public virtual ICollection<Player> Players { get; set; }
    }
}
