﻿using BasketballProject.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BasketballProject.Models
{
    public class Coach : Person
    {
        public int WorkExpirienceYears { get; set; }
    }
}
