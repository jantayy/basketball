﻿using BasketballProject.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace BasketballProject
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Coach> Coach{ get; set; }
        public DbSet<Team> Team { get; set; }
        public DbSet<Player> Player { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(@"C:\GitRepositories\basketball\BasketballProject\BasketballProject");
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Team>()
                .HasIndex(x => x.Name)
                .IsUnique();
            modelBuilder
                .Entity<Player>()
                .HasIndex(x => x.JerseyNumber)
                .IsUnique();
            modelBuilder
                .Entity<Player>()
                .HasIndex(x => x.TeamId)
                .IsUnique();
        }
    }
}
