﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BasketballProject.Enums
{
    public enum Conference
    {
        Western = 1,
        Eastern = 2
    }
}
